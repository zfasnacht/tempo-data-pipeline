import xarray as xr
from tempo_pipeline_tools import *


year = 2024
month = 2
day = 26


l1b_metadata = grab_tempo_data('TEMPO_NO2_L2',datetime.datetime(year,month,day),datetime.datetime(year,month,day)+datetime.timedelta(days=1))

for metadata in l1b_metadata:
    print(metadata)

    group = 'geolocation'
    data = xr.open_dataset('reference://', **{
        'engine'         : 'zarr',
        'group'          : group,
        'backend_kwargs' : {
            'storage_options' : {'fo': metadata},
            'consolidated'    : False,
        }})
        
    lat = data['latitude'].values
    lon = data['longitude'].values

