import botocore, aiobotocore, requests, json
import numpy as np
from pathlib import Path
import datetime
import xarray as xr
import earthaccess
from urllib.parse import urlparse
from tqdm import tqdm 
import kerchunk.hdf
import fsspec
import ujson
from multiprocessing import Pool

from functools import partial
from tqdm import tqdm 

import concurrent.futures
import kerchunk.hdf
import fsspec
import ujson

def process_url(url, folder, inline_threshold=250, **open_kws):
    filename = Path(folder).joinpath('metadata', f'{Path(url).name}.json')
    if not filename.exists():
        with fsspec.open(url, mode='rb', **open_kws) as in_file:
            meta = kerchunk.hdf.SingleHdf5ToZarr(in_file, url, inline_threshold=inline_threshold)
            with filename.open('wb') as out_file:
                out_file.write( ujson.dumps(meta.translate()).encode() )
    return filename.as_posix()

def create_metadata(
    s3_urls            : list = str, 
    folder             : str  = './',
    default_fill_cache : bool = False,
    default_cache_type : str  = 'first',
    inline_threshold   : int  = 250,
    parallel           : int = None,
) -> list:
    """ Create zarr metadata for the give S3 urls.
    
    Parameters
    ----------
    s3_urls : list[str]
        S3 urls to read and create metadata for.
    folder  : str
        Folder to store the metadata folder in (default current directory).
    default_fill_cache : bool
        If seeking to new a part of the file beyond the current buffer,
        with this True, the buffer will be filled between the sections to
        best support random access. When reading only a few specific chunks
        out of a file, performance may be better if False. In other words,
        False means that data between two chunks are not cached; i.e. 
        accessing chunk 1 and then chunk 5 will not cache chunks 2,3,4. 
    default_cache_type : str
        'first' will cache only the first block.
        See:
            https://filesystem-spec.readthedocs.io/en/latest/api.html#readbuffering
            https://filesystem-spec.readthedocs.io/en/latest/_modules/fsspec/caching.html#register_cache
    inline_threshold   : int
        Inline threshold adjusts the size below which binary blocks are 
        included directly in the json output. A higher inline threshold 
        can result in a larger json file, but faster loading time.
    parallel           : int | None
        Number of workers to create metadata in parallel (default of
        None does not use multiprocessing). Ensure the instance you're
        running on has enough memory!
        
    """

    kwargs = {
        'folder'             : folder,
        'inline_threshold'   : inline_threshold,
        'default_fill_cache' : default_fill_cache,
        'default_cache_type' : default_cache_type,
    }

    Path(folder).joinpath('metadata').mkdir(exist_ok=True, parents=True)
    filenames = set()

    if parallel is not None:
        process = partial(process_url, **kwargs)
        with concurrent.futures.ProcessPoolExecutor(parallel) as executor:
            for filename in tqdm(executor.map(process, s3_urls), desc='Creating metadata'): 
                filenames.add(filename)
    else:
        for url in tqdm(list(s3_urls), desc='Creating metadata'):
            filenames.add( process_url(url, **kwargs) )
    return list(filenames)

def get_s3_url(result: dict) -> dict:
    """ Extract the S3 download information for the given search result """
    
    def get_key(d: dict, key: str, val_type: type):
        """ Extract values for the requested key, checking expected type """
        assert(isinstance(d, dict)), f'{type(d)} given when fetching key {key}'
        assert(key in d),            f'Expected "{key}" key; found: {d.keys()}'
        assert(isinstance(d[key], val_type)), (
            f'Expected {val_type} for value of "{key}"; found: {type(d[key])}')
        return d[key]
    
    # Extract relevant nested values
    umm  = get_key(result, 'umm',         dict)
    urls = get_key(umm,    'RelatedUrls', list)
    
    # Extract only the S3 url for the granule
    get_url = lambda url: get_key(url, 'URL', str)
    is_s3   = lambda url: url.startswith('s3://') and url.endswith('.nc')
    url     = list(filter(is_s3, map(get_url, urls)))
    assert(len(url) >= 1), f'No S3 urls found: {urls}'
    assert(len(url) == 1), f'Found multiple S3 urls: {url}'
    return url[0]


def parse_s3_url(url: str) -> dict:
    """ Parse bucket/key for the given url """
    parsed = urlparse(url, allow_fragments=False)
    return { 'Bucket' : parsed.netloc,
             'Key'    : parsed.path.lstrip('/') }

def add_credential_refresh_to_session(session, sts_endpoint='https://data.asdc.earthdata.nasa.gov/s3credentials'):
    """ Session initializer that adds refreshing credentials to a session.
    
    Any time a (aio)botocore Session is created, credentials which refresh 
    automatically upon expiration are added to the session. 
    
    EDC requires AWS STS credentials for data access. The Cumulus STS endpoint
    allows us to use our EDL credentials to obtain them. Our EDL credentials 
    are in our https session so the STS endpoint will recognize that and use 
    them to return us STS credentials.

    Here we use an undocumented function in the botocore library that allows us
    to auto-refresh our credentials. This will allow you to maintain your 
    session for more than the 1 hour limit.

    See https://github.com/boto/botocore/blob/e8155d6005a878b86bfdcd2823b41f7e2d6cde08/botocore/credentials.py#L444

    This feature will automatically refresh your STS tokens prior to their 
    expiration without the need for intervention.

    """
    
    def get_credentials():
        """ Define S3 authentication credentials """
        credentials = json.loads(requests.get(sts_endpoint).content)
        return {
            'access_key'  : credentials.get('accessKeyId'),
            'secret_key'  : credentials.get('secretAccessKey'),
            'token'       : credentials.get('sessionToken'),
            'expiry_time' : credentials.get('expiration')
        }

    # Provide async version for session if necessary
    Refreshable = botocore.credentials.RefreshableCredentials
    if isinstance(session, aiobotocore.session.AioSession):
        Refreshable = aiobotocore.credentials.AioRefreshableCredentials

    # Set a RereshableCredentials object that uses our get_credentials function
    session._credentials = Refreshable.create_from_metadata(**{
        'metadata'      : get_credentials(),
        'refresh_using' : get_credentials,
        'method'        : 'sts-assume-role',
    })
    print(f'Added refreshable credentials to {session}')


def grab_tempo_data(shortname,date_1,date_2,parallel=None,bounding_box=(),version=2):


    botocore.register_initializer(add_credential_refresh_to_session)

    # First login will request earthdata credentials and persist them to ~/.netrc                                                                                                                                   
    # Subsequent logins will then use these stored credentials                                                                                                                                                      
    if Path('~/.netrc').exists(): earthaccess.login(strategy="netrc")
    else:                         earthaccess.login(strategy="interactive", persist=True)

    if len(bounding_box) == 0:
        search_kwargs = {
            'short_name'   : shortname,
            'version'      : 'V0'+str(version),
            'temporal'     : (date_1.strftime('%Y-%m-%d'),date_2.strftime('%Y-%m-%d')),
        }
    elif len(bounding_box) == 4:
        search_kwargs = {
            'short_name'   : shortname,
            'version'      : 'V0'+str(version),
            'bounding_box' : bounding_box,
            'temporal'     : (date_1.strftime('%Y-%m-%d'),date_2.strftime('%Y-%m-%d')),
        }
    else:
        print('Bounding Box needs to be 4 elements')
        exit()
        
    results = earthaccess.search_data(cloud_hosted=True, count=200, **search_kwargs)
    s3_urls = list(map(get_s3_url, results))

    metadata = create_metadata(s3_urls, parallel=parallel,folder='/data/1002/omi/TEMPO/metadata/'+shortname)

    return metadata
