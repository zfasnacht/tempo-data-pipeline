# TEMPO Data Pipeline

This repository shows the implementation of a python pipeline for grabbing TEMPO data from the Earthdata Cloud. The core software was developed by Brandon Smith (SSAI). 

This tool requires some non-standard python libraries to be installed through pip, so to run on a machine like the TLCF you'll need your own anaconda install 

pip install earthaccess aiobotocore kerchunk

The file tempo_pipeline_libraries.py includes the libraries for access the AWS S3 data with a function called "grab_tempo_data" to get the desired TEMPO data files. These libraries can be imported into any python software (as long as they are in the same directory or in PYTHONPATH) simply by: 

```
from tempo_pipeline_libraries import * 
```

The 'grab_tempo_data' function is defined below 

```
grab_tempo_data(tempo_product_shortname,date_1,date_2,parallel=None,bounding_box=(),version=2)
```

where 

tempo_product_shortname (str) - defines the shortname of the desired product (such as TEMPO_RAD_L1, TEMPO_NO2_L2, TEMPO_CLDO4_L2, etc)

date_1, date_2 (python datetime objects) - define the range of desired TEMPO products (note that the function only pulls 100 files at a time, so the range shouldn't be too large )

parallel (int or None) - defines how many processors the function should be run on. When the software is run for the first time, it grabs the files out of AWS and stores the metadata locally. As a result, using multiple cores when the metadata are first pulled is beneficial (ideally no more than 6 processors). After the initial call of the function, the metadata are stored locally and called from the local directory moving forward which makes things very fast and removes the need for running on multiple processors. 

bounding box (empty tuple or tuple len 4 (min_lon, min_lat, max_lon, max_lat)) - can be given as a keyword if you only need TEMPO granules from a specific geographic region 

version (int) - simply defines the version of TEMPO data being requested 

The code grab_tempo_no2.py shows a simple example of using the software pipeline to grab Level 2 TEMPO NO2 files and reading in the latitude/longitude 


